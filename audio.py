import array
import math
import pyaudio
import sys
import time

class Voice:
    def __init__(self, frame_rate):
        self.current_frame = 0
        self.frame_rate = frame_rate

    def generate(self, n):
        data = make_frames(self.current_frame, n, self.frame_rate)
        self.current_frame += n
        return data

def square(x, duty = 0.5):
    x1 = x % 1
    return -1 if x1 < duty else 1

def triangle(x):
    x1 = x % 1
    if x1 < 0.5:
        return 4 * x1 - 1
    else:
        return 3 - 4 * x1

def xsin(x):
    return math.sin(2 * math.pi * x)

def A(t, level = 16384, decay_rate = 3):
    return level * math.exp(-decay_rate * t)

def make_frames(k0, n, frame_rate):
    f = 440
    T = 1 / frame_rate
    F = f / frame_rate
    lf = 1
    LF = lf / frame_rate

    data = array.array('h')
    data.extend(int(A(T * k) * square(k * F,
                                      duty = (.5 + .33 * xsin(k * LF))))
                for k in range(k0, k0 + n))
    return data.tobytes()

def main():
    frame_rate = 44100

    p = pyaudio.PyAudio()
    v = Voice(frame_rate)

    def callback(in_data, frame_count, time_info, status):
        data = v.generate(frame_count)
        return (data, pyaudio.paContinue)

    stream = p.open(format = pyaudio.paInt16,
                    channels = 1,
                    rate = frame_rate,
                    output = True,
                    stream_callback = callback)

    stream.start_stream()

    while stream.is_active():
        time.sleep(1)

    stream.stop_stream()
    stream.close()

    p.terminate()

if __name__ == '__main__':
    main()
